# Reveup Client Backend

## Dependencies
* node
* npm
* mongodb


## How to run

* make sure that mongo db is running
* run mongo db in docker
* docker pull mongo
* docker run -d -p 27017-27019:27017-27019 --name mongodb mongo
* change db host and post if mongo db's host and port are deferent in config.js

if the docker image is pulled once 
* docker run -d -p 27017-27019:27017-27019 mongo


* install npm packages

```node
npm install
```

* run back end

```node
node server.js
```
* api will be served at

```node
localhost:[port]/api/v1/
```



# APIS
