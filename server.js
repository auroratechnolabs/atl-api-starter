const express = require( 'express' );

const app = express();
const bodyParser = require( 'body-parser' );
const cors = require( 'cors' );
const mongoose = require( 'mongoose' );
const winston = require( 'winston' );

// config
const config = require( './config' );

// routes imports
const vehicleRoutes = require( './routes/vehiclesRoutes' );


// Todo move this to a singleton object and use ( important ) replace console logs with this
const logger = winston.createLogger( {
	levels : winston.config.syslog.levels,
	transports : [
		new winston.transports.Console( { level : 'error' } ),
		new winston.transports.File( {
			filename : 'combined.log',
			level : 'info',
		} ),
	],
} );

app.use( cors() );


app.use( bodyParser.json() );

mongoose.connect( `mongodb://${config.db.host}:${config.db.port}${config.db.dbPath}`, { useNewUrlParser : true } );
const { connection } = mongoose;

connection.once( 'open', () => {
	console.log( 'MongoDB database connection established successfully' );
} );

app.use( '/api/v1/vehicles', vehicleRoutes );

// error handler
app.use( ( error, req, res, next ) => { // eslint-disable-line no-unused-vars
	if ( error.isServer ) {
		// log server errors 5xx status codes
		logger.error( error );
		return res.status( 500 ).json( 'internal server error' );
	}

	return res.status( 500 ).json( 'internel server error' );
} );

app.listen( config.serverPort, () => {
	console.log( `Server is running on Port: ${config.serverPort}` );
} );
