const mongoose = require( 'mongoose' );

const { Schema } = mongoose;

const Vehicle = new Schema( {
	name : {
		type : String,
	},
	chassisNumber : {
		type : String,
	},
	registrationNumber : {
		type : String,
	},
	type : {
		type : String,
	},
	make : {
		type : String,
	},
	model : {
		type : String,
	},
	trimLevel : {
		type : String,
	},
	province : {
		type : String,
	},
	imgUrl : {
		type : [String],
	},
	milage : {
		// used to idetify
		type : Number,
	},
	color : {
		type : String,
	},
	assignments : {
		type : [
			String,
		],
	},
	yom : {
		type : Number,
	},
	note : {
		type : String,
	}
} );

module.exports = mongoose.model( 'Vehicle', Vehicle );
