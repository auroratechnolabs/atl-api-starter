const boom = require( 'boom' );
const Vehicle = require( '../models/vehicle.model' );
const constants = require( '../constants' );

module.exports.createVehicle = async ( req, res, next ) => {
	const vehicle = new Vehicle( {
		...req.body,
	} );

	vehicle.save()
		.then( ( savedVehicle ) => {
			res.status( 200 ).json( {
				note : 'vechicle added successfully',
				id : savedVehicle.id,
			} );
		} )
		.catch( ( err ) => {
			next( boom.internal( err ) );
		} );
};

module.exports.updateVehicle = async ( req, res, next ) => {
	const { content } = req.body;
	// TO DO use userId for below requirenment
	// const { userId } = req.body;
	const { id } = req.body;

	Vehicle.findById( id, ( error, vehicle ) => {
		// vehicle not found or found vehicle is not belong to the user
		// TODO : write this logic
		if ( false ) { next( boom.notFound() ); } else { // eslint-disable-line 
			// TODO look at this again
			vehicle.content = { ...content };

			vehicle
				.save()
				.then( ( savedVehicle ) => {
					res.json( {
						note : 'Vehicle updated!',
						id : savedVehicle.id,
					} );
				} )
				.catch( ( err ) => {
					next( boom.internal( err ) );
				} );
		}
	} );
};

module.exports.getById = async ( req, res, next ) => {
	const { id } = req.params;

	Vehicle.findById( id, ( err, vehicle ) => {
		if ( err ) next( boom.notFound() );

		// note not found or found note is not belong to the user
		else if ( !vehicle ) next( boom.notFound() );
		else res.json( vehicle );
	} );
};

module.exports.getByRegNumber = async ( req, res, next ) => {
	const { registrationNumber } = req.params;

	Vehicle.findOne( { registrationNumber }, ( err, vehicle ) => {
		if ( err ) {
			next( boom.internal( err ) );
		} else {
			res.json( vehicle );
		}
	} );
};

module.exports.getPaginatedVehicles = async ( req, res, next ) => {
	const limit = Math.abs( req.query.limit ) || constants.vehiclesPerPage;
	const page = ( Math.abs( req.query.page ) || 1 ) - 1;

	Vehicle.find()
		.limit( limit )
		.skip( limit * page )
		.exec( ( err, vehicles ) => {
			if ( err ) {
				next( boom.internal( err ) );
			} else {
				Vehicle.count().exec( ( error, count ) => {
					res.json( {
						vehicles,
						page,
						pages : count / constants.vehiclesPerPage,
					} );
				} );
			}
		} );
};
