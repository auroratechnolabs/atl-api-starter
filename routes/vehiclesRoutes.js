const express = require( 'express' );

const vehicleValidator = require( '../validators/vehicle.validator' );
const vehicleService = require( '../services/vehicle.service' );

const vehicleRoutes = express.Router();

// create a new vehicle
vehicleRoutes.route( '/create' ).post(
	vehicleValidator.validateCreateVehicle,
	vehicleService.createVehicle,
);

// update a new vehicle
vehicleRoutes.route( '/update' ).patch(
	vehicleValidator.validateUpdateVehicle,
	vehicleService.updateVehicle,
);

// get vehicle by id
vehicleRoutes.route( '/getById/:id' ).get(
	vehicleValidator.validateGetVehicleById,
	vehicleService.getById,
);

// get vehicle by registration number
vehicleRoutes.route( '/getByRegNumber/:registrationNumber' ).get(
	vehicleValidator.validateGetVehicleByRegNumber,
	vehicleService.getByRegNumber,
);

vehicleRoutes.route( '/all' ).get(
	vehicleValidator.validateGetVehicles,
	vehicleService.getPaginatedVehicles,
);

module.exports = vehicleRoutes;
