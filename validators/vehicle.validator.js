const Joi = require( 'joi' );
const boom = require( 'boom' );

module.exports.validateCreateVehicle = ( req, res, next ) => {
	const bodySchema = Joi.object( {
		name : Joi.string().required(),
		chassisNumber : Joi.string().required(),
		registrationNumber : Joi.string().required(),
		type : Joi.string().required(),
		make : Joi.string().required(),
		model : Joi.string().required(),
		trimLevel : Joi.string(),
		province : Joi.string().required(),
		imgUrl : Joi.string(),
		milage : Joi.number().required(),
		color : Joi.string().required(),
		yom : Joi.number().required(),
		note : Joi.string(),
	} ).unknown();

	try {
		const value = bodySchema.validate( req.body );

		if ( value.error ) {
			next( boom.badRequest( value.error, {} ) );
		} else next();
	} catch ( error ) {
		next( boom.internal( error ) );
	}
};

module.exports.validateUpdateVehicle = ( req, res, next ) => {
	const bodySchema = Joi.object( {
		id : Joi.string().required(),
		content : Joi.object( {
			name : Joi.string().required(),
			chassisNumber : Joi.string().required(),
			registrationNumber : Joi.string().required(),
			type : Joi.string().required(),
			make : Joi.string().required(),
			model : Joi.string().required(),
			trimLevel : Joi.string(),
			province : Joi.string().required(),
			imgUrl : Joi.string(),
			milage : Joi.number().required(),
			color : Joi.string().required(),
			yom : Joi.number().required(),
			note : Joi.string(),
		} ).unknown(),
	} ).unknown();

	try {
		const value = bodySchema.validate( req.body );

		if ( value.error ) {
			next( boom.badRequest( value.error, {} ) );
		} else next();
	} catch ( error ) {
		next( boom.internal( error ) );
	}
};

module.exports.validateGetVehicleById = ( req, res, next ) => {
	const bodySchema = Joi.object( {
		id : Joi.string().required(),
	} ).unknown();

	try {
		const value = bodySchema.validate( req.params );
		if ( value.error ) {
			next( boom.badRequest( value.error, {} ) );
		} else next();
	} catch ( error ) {
		next( boom.internal( error ) );
	}
};

module.exports.validateGetVehicleByRegNumber = ( req, res, next ) => {
	const bodySchema = Joi.object( {
		registrationNumber : Joi.string().required(),
	} ).unknown();

	try {
		const value = bodySchema.validate( req.params );
		if ( value.error ) {
			next( boom.badRequest( value.error, {} ) );
		} else next();
	} catch ( error ) {
		next( boom.internal( error ) );
	}
};

module.exports.validateGetVehicles = ( req, res, next ) => {
	const bodySchema = Joi.object( {} ).unknown();

	try {
		const value = bodySchema.validate( req.params );
		if ( value.error ) {
			next( boom.badRequest( value.error, {} ) );
		} else next();
	} catch ( error ) {
		next( boom.internal( error ) );
	}
};
