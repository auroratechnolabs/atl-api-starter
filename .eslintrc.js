module.exports = {
	'env' : {
		'mocha' : true,
	},
	'extends' : 'nukedzn',
	'parserOptions' : {
		'ecmaVersion' : 2018,
	},
};